import requests
from bs4 import BeautifulSoup
import selenium
import os
from selenium import webdriver
import urllib.request as ur
import time
from selenium.webdriver.common.by import By
'''
https://www.learncodewithmike.com/2020/05/python-selenium-scraper.html
https://ithelp.ithome.com.tw/articles/10193755
https://github.com/facebookarchive/python-instagram
https://blog.csdn.net/lA6Nf/article/details/79338920?utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-3.control&dist_request_id=1330147.33306.16182065924742055&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromMachineLearnPai2%7Edefault-3.control
https://medium.com/marketingdatascience/selenium教學-一-如何使用webdriver-send-keys-988816ce9bed
'''

def picture_info(soup_location_page,limit=None):
    pic_gref=[]
    pic=soup_location_page.select("div.v1Nh3.kIKUG._bz0w a")
    #both hot and new !!!
    for i in pic :
        if len(pic_gref) == limit:
            break
        pic_gref.append(i['href'])
    print(len(pic_gref))
    return pic_gref

def location_lat_lng(soup_location_page):
    loc_lat = soup_location_page.find("meta",  {"property":"place:location:latitude"})['content']
    loc_lng = soup_location_page.find("meta",  {"property":"place:location:longitude"})['content']
    print( "lat= ",loc_lat)
    print( "lng= ",loc_lng)
    return loc_lat,loc_lng

def writefile(things,path,name):
    # writefile(str(loc_page),"/Users/weikai/practice/weikai_side/one","loc_page.html")
    with open (file=path+name,mode="w") as F:
        F.write(things)

def loadfile(path):
    with open (file=path,mode='r',encoding='UTF-8') as F:
        return F.read()



##Make sure Chrome  won't broke
# options = webdriver.chrome.options.Options()
# options.add_argument("--disable-notifications")

# chrome = webdriver.Chrome('/Users/weikai/practice/weikai_side/one/chromedriver_mac', chrome_options=options)

## Facebook
# chrome.get("https://www.facebook.com/")
# email = chrome.find_element_by_id('email')
# password = chrome.find_element_by_id('pass')
# email.send_keys('abnb0001@gmail.com')
# password.send_keys('bnb741963')

## ig dynamic login
# chrome.get("https://www.instagram.com/accounts/login/")
# time.sleep(3)
# username = chrome.find_element_by_name('username')
# password = chrome.find_element_by_name('password')
# username.send_keys('bookart_24')
# password.send_keys('weikk56123')
# password.submit()
# time.sleep(3)
# chrome.get('https://www.instagram.com/explore/locations/929733/')

# print("chrome.page_source  ",chrome.page_source)
# loc_page = BeautifulSoup(chrome.page_source,features="html.parser")

# location_lat_lng( loc_page ) 
# picture_info( loc_page )

# b = input("enter to exit")
# chrome.quit()





def request_soup(urls):
    r = requests.get(url=urls)
    return BeautifulSoup(r.text,features="html.parser")


# def login():
#     https://www.instagram.com/accounts/login/

def Main():
    #local test
    paths="/Users/weikai/practice/weikai_side/one/oneloc_page.html"
    abs_path=os.path.join(paths)
    loc_page = BeautifulSoup(open(abs_path, encoding="utf-8"), "html.parser")
    
    #API test ?
    # loc_page=request_soup("https://www.instagram.com/explore/locations/929733/")

    # print(loc_page)
    # print('title ',loc_page.title)

    #crew lat,lng
    location_lat_lng( loc_page ) 

    #location page picture 
    picture_info( loc_page , 3 )
    # print(loc_lat,loc_lng)

    # print( loc_page.find("div" ) )

    #crew post 

    pass



if __name__ == "__main__":
    Main()